<?php

include_once('../nlp-test/ApplyNLP.php');
class XMLGlobal
{
	var $h;
	var $m;
	var $s;
	
	var $xml_file;
	var $start_time;
	
	function setXMLFile($__filename)
	{
		$this->xml_file = $__filename;
	}
	
	function setStartTime() {
		$this->start_time =  XMLGlobal::_getMicroTime();
	}
	

	function _getMicroTime(){ 
		list($usec, $sec) = explode(' ', microtime()); 
		return ((float)$usec + (float)$sec); 
	}
	

	function _echoStatus($__start_time, $__count_rows, $__milestone){
		$end_time = $this->_getMicroTime();
		$time = $__start_time - $end_time;
		$dot = strrpos($time, '.');
		$script_time = abs(substr($time, 0, $dot + 2));
		
		$this->_splitTime($script_time);
		
		Basic::printToConsole("\n", false);
		Basic::printToConsole($__milestone . "\n", false);
		Basic::printToConsole('Time and date: [' . date('m/d/y - H:i:s') . "]\n", false);
		Basic::printToConsole('Run-time for the script: ' . $this->h . ':' . $this->m . ':' . $this->s, false);
		Basic::printToConsole('Rows inserted: ' . $__count_rows . "\n");
		flush();
	}

	function _splitTime($__script_time)
	{
		$this->h = '0'; 
		$this->m = '0'; 
		$this->s = intval($__script_time % 60);
		
	 	if($__script_time > 60) {
			$this->m = intval($__script_time / 60) % 60;
		}
		
	 	if($__script_time > 3600) { 
			$this->h = intval($__script_time / 3600);
		}
	}

	function _startToParse()
	{

		$this->xml_parser = xml_parser_create();
		xml_set_object($this->xml_parser, $this);

		xml_parser_set_option($this->xml_parser, XML_OPTION_SKIP_WHITE, true);

		xml_parser_set_option($this->xml_parser, XML_OPTION_CASE_FOLDING, false); 
	
		xml_set_element_handler($this->xml_parser, "_startTagProcessor", "_endTagProcessor");
		xml_set_character_data_handler($this->xml_parser, "_charDataProcessor");
		

		if (!($fp = fopen($this->xml_file, 'r'))) 
		{
		      basic::error('Fatal error', "File I/O error: $this->xml_file"); 
		} 
		
		//Parse XML 
		while ($data = fread($fp, 4096)) 
		{
		      //error... :( 
		      if (!xml_parse($this->xml_parser, $data, feof($fp))) 
		      {
		            $ec = xml_get_error_code($this->xml_parser); 
		            basic::error('Fatal error', "XML parser error (error code " . $ec . "): " . xml_error_string($ec) . 
		"\nThe error was found on line: " . xml_get_current_line_number($this->xml_parser)); 
		      } 
		}


		//free your mind, and the rest will follow :)
		xml_parser_free($this->xml_parser);
	}
	
}


class ParseXMLStructure extends XMLGlobal
{
	var $count_rows; 
	var $count_rows_temp; 	
	var $current_tag;
	var $permitted_tags;
	var $topic;
	var $catid;
	var $title;
	var $description;
	var $last_update;
		
	var $type;
	var $resource;

    var $tempcount=0;
    var $query;
	

	function startParse() 
	{
		//Starts with clean properties
		$this->current_tag = '';
		$this->topic = '';
		$this->catid = '';
		$this->title = '';
		$this->last_update = '';
		$this->description = '';
		$this->type = '';
		$this->resource = '';


		//$this->permitted_tags = array('narrow', 'narrow1', 'narrow2');
        $this->permitted_tags = array();

		$this->_startToParse(); //Start to parse function [located in XMLGlobal]

        if($this->query!='')
        {
            Database::sqlWithoutAnswer($this->query);

        }

		Basic::printToConsole("Finished processing structure RDF file!\nIt took " . $this->h . ' hours, ' . $this->m . ' minutes and ' . $this->s . " seconds\nInserted rows into the database: " . $this->count_rows . "\n");
	}
	

	function _startTagProcessor($__parser, $__tag_name, $__attributes)
	{

		$this->current_tag = $__tag_name;

		if(strtolower($this->current_tag) == 'topic') 
		{
			//If it's true get id
			$this->topic = $__attributes['r:id'];
		}
		
		if(in_array(strtolower($this->current_tag), $this->permitted_tags))
		{
			//Set type to be equal with the name
			$this->type = $__tag_name;
			
			//Set the resource to be equal the resource found in the tag
			$this->resource = $__attributes['r:resource'];
		}
	}

	function _endTagProcessor($parser, $__tag_name)
	{
        //sprint_r($this);

		//Check if the end tag is topic, if it is it run a SQL query
		if(strtolower($__tag_name) == 'topic' && (strpos(addslashes($this->topic),'op/Computers') || strpos(addslashes($this->topic),'op/Sports') ||  strpos(addslashes($this->topic),'op/Science')))
		{
            $count=substr_count($this->topic,'/');
           // print_r(addslashes($this->topic));

            if($this->tempcount==0)
            {
                $this->query = 'INSERT INTO structure';
                $this->query .= '(catid,name,level)';
                $this->query .= ' VALUES("' . $this->catid . '", "' . addslashes($this->topic) . '", "' . $count . '")';
                $this->tempcount++;
            }
            else
            {

                $this->query .= ' ,("' . $this->catid . '", "' . addslashes($this->topic) . '", "' . $count . '")';
                $this->tempcount++;
            }


            if($this->tempcount==9000)
            {
                echo $this->query;

                Database::sqlWithoutAnswer($this->query);
                $this->tempcount=0;
                $this->query='';
            }
			$this->count_rows++; //Count rows
            echo $this->count_rows." rows inserted \n";
			$this->count_rows_temp++; //Temporary count rows - used to make a milestone
			$query = '';

			//Reset the tags
			$this->catid = '';
			$this->topic = '';
			$this->title = '';
			$this->description = '';
			$this->last_update = '';
			$this->current_tag = '';
		}
		
		/*if(in_array(strtolower($this->current_tag), $this->permitted_tags))
		{
			$query = 'INSERT INTO datatypes';
			$query .= '(catid,type,resource)';
			$query .= ' VALUES("' . $this->catid . '", "' . addslashes($this->type) . '", "' . addslashes(trim($this->resource)) . '")';
			Database::sqlWithoutAnswer($query);

			$this->count_rows++; //Count rows
            echo $this->count_rows." rows inserted \n";
			$this->count_rows_temp++; //Temporary count rows - used to make a milestone
			$query = '';
			$this->type = '';
			$this->resource = '';		
			$this->current_tag = '';
		}*/
		
		//Check if the stats are set
		if(ECHO_STATS) {			
			//Check if ECHO_STATS_FREQUNCY is reached
			if($this->count_rows_temp >= ECHO_STATS_FREQUNCY)
			{
				$this->count_rows_temp = 0;
				$this->_echoStatus($this->start_time, $this->count_rows, 'Yet another '.ECHO_STATS_FREQUNCY.' rows reached! - structure RDF document');
			}
		}
			
	}
	

	function _charDataProcessor($__parser, $__data) 
	{
		//Checks if there is something between the tags
		if(trim($__data) != '')
		{
           // echo $this->current_tag;
			//Finds out what kind of data it is
			switch($this->current_tag) {
				case 'catid':
					$this->catid .= $__data;
					break;
				case 'd:Title':
					$this->title .= $__data;
					break;
				case 'd:Description':
					$this->description .= $__data;
					break;
				case 'lastUpdate':
					$this->last_update .= $__data;
					break;
				default:
				break;
			}
		}
	}
		
}


class ParseXMLContent extends XMLGlobal 
{
	var $count_rows;
	var $count_rows_temp;
	
	var $current_tag;
	var $permitted_tags;
	
	//(content_links)
	var $topic;
	var $type;
	var $resource;
	var $catid;
	
	//(content_description)
	var $external_page;
	var $title;
	var $description;
	var $ages;
	var $mediadate;
	var $priority;
    var $tempcount=0;
    var $query;
    var $nlp;
	function startParse() 
	{
        $this->nlp=new ApplyNLP();
		//Starts with clean properties
		//(content_links)
		$this->topic = '';
		$this->type = '';
		$this->resource = '';
		
		//(content_description)
		$this->external_page = '';
		$this->title = '';
		$this->description = '';
		$this->ages = '';
		$this->mediadate = '';
		$this->priority = '';
		
		$this->current_tag = '';
		

		$this->permitted_tags = array('link', 'link1');
		
		$this->_startToParse();

        if($this->query!='')
        {
            Database::sqlWithoutAnswer($this->query);

        }
		Basic::printToConsole("Finished processing content RDF file!\nIt took " . $this->h . ' hours, ' . $this->m . ' minutes and ' . $this->s . " seconds\nInserted rows into the database: " . $this->count_rows . "\n");
	}
	

	function _startTagProcessor($__parser, $__tag_name, $__attributes)
	{
		
		//Sets what tag we currenly are in
		$this->current_tag = $__tag_name;
   //     print_r($__tag_name);
		
		//Check if the current tag is topic

        if(strtolower($this->current_tag) == 'topic' && isset($__attributes['r:id']))
		{
			//Reset catid
			$this->catid = '';

			//If it's true get id
           // print_r($__attributes);
			$this->topic = $__attributes['r:id'];
            //print_r($this->topic);
		}
		
		//Check if the current tag is external page
		if(strtolower($this->current_tag) == 'externalpage') 
		{
			//If it's true get id
			$this->external_page = $__attributes['about'];
		}
		
		//Check if the tag is equal to some of our permitted tags
		if(in_array(strtolower($this->current_tag), $this->permitted_tags)) 
		{
			//Set type to be equal with the name
			$this->type = $__tag_name;

			//Set the resource to be equal the resource found in the tag
			$this->resource = $__attributes['r:resource'];
		}
	}

	function _endTagProcessor($__parser, $__tag_name)
	{
       // echo $this->description;
		//Check if the end tag is external_page
		if(strtolower($__tag_name) == 'externalpage')
		{
           // print_r($this->catid);
            if(strpos(addslashes($this->topic),'op/Science') || strpos(addslashes($this->topic),'op/Sports') || strpos(addslashes($this->topic),'op/Computers'))
            {
                $description=$this->nlp->process(addslashes(trim($this->description)));

                $description= mysql_real_escape_string (json_encode($description));

                if($this->tempcount==0)
                {

                    $this->query = 'INSERT INTO content_description';
                    $this->query .= '(catid, topic, link, description)';
                    $this->query .= ' VALUES("' . addslashes($this->catid) . '", "' . addslashes($this->topic) . '", "'. addslashes($this->external_page) . '",  "' . ($description) . '")';
                    $this->tempcount++;
                }
                else
                {
                    $this->query .= ' ,("' . addslashes($this->catid) . '", "' . addslashes($this->topic) . '", "'. addslashes($this->external_page) . '",  "' .($description). '")';
                    $this->tempcount++;
                }
                if($this->tempcount==10000)
                {
                    //echo $this->query;

                    Database::sqlWithoutAnswer($this->query);
                    $this->tempcount=0;
                    $this->query='';
                }
                $this->count_rows++; //Count rows
                $this->count_rows_temp++; //Temporary count rows - used to make a milestone
                $query = '';
                $this->external_page = '';
                $this->title = '';
                $this->description = '';
                $this->ages = '';
                $this->mediadate = '';
                $this->priority = '';
                $this->current_tag = '';
                $this->type = '';
                $this->resource = '';
                $this->current_tag = '';
		    }
        }
		
		//Check if the end tag is in the range of permitted tags
		/*if(in_array(strtolower($__tag_name), $this->permitted_tags))
		{
			$query = 'INSERT INTO content_links';
			$query .= '(catid, topic, type, resource)';
			$query .= ' VALUES("' . addslashes($this->catid) . '", "' . addslashes($this->topic) . '", "' . addslashes($this->type) . '", "' . addslashes($this->resource) . '")';
			Database::sqlWithoutAnswer($query);
			$this->count_rows++; //Count rows
			$this->count_rows_temp++; //Temporary count rows - used to make a milestone
			$query = '';
			$this->type = '';
			$this->resource = '';		
			$this->current_tag = '';
		}*/
		
		//Check if the stats are set
		if(ECHO_STATS) {			
			//Check if ECHO_STATS_FREQUNCY is reached
			if($this->count_rows_temp == ECHO_STATS_FREQUNCY)
			{
				$this->count_rows_temp = 0;
				$this->_echoStatus($this->start_time, $this->count_rows, 'Yet another '.ECHO_STATS_FREQUNCY.' rows reached! - content RDF document');
			}
		}
	}

	function _charDataProcessor($__parser, $__data) 
	{
		//Checks if there is something between the tags
		if(trim($__data) != '')
		{
		//Finds out what kind of data it is
		switch($this->current_tag) {
			case 'catid':
				$this->catid = $__data;
				break;
			case 'd:Title':
				$this->title = $__data;
				break;
			case 'd:Description':
				$this->description = $__data;
				break;
			case 'ages':
				$this->ages .= $__data;
				break;
			case 'mediadate':
				$this->mediadate .= $__data;
				break;
			case 'priority':
				$this->priority .= $__data;
				break;
			default:
			break;
		}
		}
	}
	
		
}


?>
