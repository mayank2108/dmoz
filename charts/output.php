<html>
<head>
    <title>chart created with amCharts | amCharts</title>
    <meta name="description" content="chart created using amCharts live editor" />

    <!-- amCharts javascript sources -->
    <script src="amcharts.js" type="text/javascript"></script>
    <script src="serial.js" type="text/javascript"></script>
    <script src="dark.js" type="text/javascript"></script>
</head>

<body>



</body>

</html>
<?php
session_start();

if(isset($_SESSION['data']))
{
//    print_r($_SESSION['data']);

    $i=0;

    foreach($_SESSION['data'] as $level)
    {
        if(!empty($level))
        {
            ?>

            </br>
            Cosine Similarity comparison at level <?php echo $i+1?>

            </br>

            <div id="chartdiv<?php echo $i?>" style="width: 100%; height: 400px; background-color: #282828;" >

            </div>
            <script type="application/javascript">var data = JSON.parse(<?php echo "'". json_encode($level,true)."'" ?>);

                AmCharts.makeChart("chartdiv<?php echo $i?>",
                    {
                        "type": "serial",
                        //"pathToImages": "http://cdn.amcharts.com/lib/3/images/",
                        "categoryField": "category",
                        "rotate": true,
                        "startDuration": 1,
                        "handDrawScatter": 3,
                        "theme": "dark",
                        "categoryAxis": {
                            "gridPosition": "start"
                        },
                        "trendLines": [],
                        "graphs": [
                            {
                                "fillAlphas": 1,
                                "id": "AmGraph-1",
                                "title": "graph 1",
                                "type": "column",
                                "valueField": "value"
                            }
                        ],
                        "guides": [],
                        "valueAxes": [
                            {
                                "id": "ValueAxis-1",
                                "logarithmic": true,
                                "title": "Similarity Index"
                            }
                        ],
                        "allLabels": [],
                        "balloon": {},
                        "titles": [
                            {
                                "id": "Title-1",
                                "size": 15,
                                "text": "Similarity Comparison"
                            }
                        ],
                        "dataProvider":data,
                        handDrawn:false,
                        creditsPosition:'bottom-right'

                    }
                );




                console.log(as);</script>
            <?php
            $i++;

        }
    }
}
?>

