<?php

class DownloadExtractFile extends Dot
{
	var $filename; 
 	var $path;
 	var $download_speed;

 	function setPath($__path) 
 	{
 		$this->path = $__path;
 	}
 	

 	function setDownloadSpeed($__speed) 
 	{
        echo "1";
 		$this->downloadSpeed = $__speed;
 	}

 	function setFilename($__filename) 
 	{
 		$this->filename = $__filename;
 	}

 	function delete() 
 	{
 		if(@!unlink($this->filename.'.u8.gz')) 
 		{
 			Basic::printToConsole("Warning: Could not delete the old gun zipped file. Maybe it isn't created yet.\n");
 		}

		if(@!unlink($this->filename)) 
 		{
 			Basic::printToConsole("Warning: Could not delete the old RDF file. Maybe it isn't created yet.\n");
 		}
 	}

 	function download()
 	{
 		$this->count = 0;
 		
 		//
 		//Download file information
 		//
 		Basic::printToConsole('Downloading file information:  ', false);
 		$filecheck = new CheckURL; //Create a new object 
    	$filecheck->downloadHeaders($this->path); //Download headers from a URL 
    	$filecheck->lastModified(); //Run the function that finds when the URL(document) was last modified. 
    	$filecheck->contentLenght(); //Run the function that finds content lenght
    	
    	$filesize = round($filecheck->content_lenght/1024); //Create a variable filesize
    	
    	Basic::printToConsole('DOWNLOADED', false);

 		$this->frequency = 100; //Set the frequency
		Basic::printToConsole("\nThe script is downloading " . $this->path . "\nThe file is downloaded with " . $this->downloadSpeed . " KB/s\nSize of the file is: $filesize KB\nThe DMOZ data dumps were last updated: " . $filecheck->last_modified ."\n\nDownloading please wait");

        $context = stream_context_create(array(
            'http' => array(
                'proxy' => '192.168.67.26:808' // The proxy server address and port
            ),
        ));

        //Open the url of the structure rdf file
		 if(!($file = file_get_contents($this->path,false,$context)))
		 {
			 basic::error('Fatal error', 'I/O error! Could not download the file');
		 }
		
		//Open the file we write to
		if(!$openfile = fopen($this->filename . ".u8.gz", 'w'))
		{
			basic::error('Fatal error', 'Cannot open the file ('.$this->filename.'.u8.gz)');
		}
		
		//Write the data
		while ($data = fread($file, round(DOWNLOAD_SPEED * 1024))) 
		{
			if(!fwrite($openfile, $data)) 
			{
				basic::error('Fatal error', 'Cannot write to file ' . $this->filename . '');
			}
			
			//Don't write the dot every time
			$this->PrintDot();
			
		}
		fclose ($file);
		fclose($openfile);
		Basic::printToConsole("\n\n " . strtoupper($this->filename) . ".U8.GZ WAS SUCCESSFUL DOWNLOADED \n Download URL: " . $this->path . "\n");
 	}

	function extract()
 	{
 		Basic::printToConsole("\n\nExtracting the gunzipped file...", False);
 		
 		 //Open the downloaded gunzip file
		 if(!($file = gzopen($this->filename.".u8.gz","r")))
		 {
		 	 basic::error('Fatal error', 'I/O error! Could not open the downloaded file!');
		 }
		 
		 		
		//Open the file we write to
		if(!$openfile = fopen($this->filename,'a'))
		{
			basic::error('Fatal error', 'Cannot open the file (' . $this->filename . ')');
		}
		
		//Write the data
		while (!gzeof($file))
		{
			$buff = gzgets($file, 4096) ;
			fputs($openfile, $buff) ;
		}                    
		
		fclose ($openfile);
		gzclose ($file);
		Basic::printToConsole("\n\n " . strtoupper($this->filename) . ".U8.GZ WAS SUCCESSFUL EXTRACTED! \n Filename: " . $this->filename . "\n");
 	}	
} 	

?>
