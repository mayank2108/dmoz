<?php
//https://github.com/ngeiswei/techtc-builder strip content and rdf files by this script

include('config.php');
require('class_command.php');
require('class_download.php');
require('class_clean.php');
require('class_parse.php');
Database::connect();
ini_set('memory_limit', '10240M');
$file = new DownloadExtractFile;
$clean_xml = new CleanXML;
$parse_structure = new ParseXMLStructure;
$parse_content = new ParseXMLContent;
if(STRUCTURE_DOWNLOAD_AND_EXTRACT) {
    echo DOWNLOAD_SPEED;
    $file->setDownloadSpeed(DOWNLOAD_SPEED); //Set download speed
    echo "sd1\n";
    echo "sd1\n";
    $file->setFilename(FILE_RDF_STRUCTURE); //Set the filename
    echo "sd1\n";
   // $file->delete(); //Delete the old file - if it's there
 //   $file->setPath('http://rdf.dmoz.org/rdf/structure.rdf.u8.gz'); //Set path of what file it is downloading.
  //  $file->download(); //Start the download
 //   $file->extract(); //Extract the file
    if(CONTENT_CLEAN) {
       //$clean_xml->cleanFile(FILE_RDF_STRUCTURE);
    }

    if(!CONTENT_PARSE_N_INSERT) {
        $parse_content->setStartTime(); //Start time
        $parse_content->setXMLFile(FILE_RDF_CONTENT); //Set what XML file to parse
        $parse_content->startParse(); //Start parsing the document
    }

    if(STRUCTURE_PARSE_N_INSERT) {
        $parse_structure->setStartTime(); //Start time
        $parse_structure->setXMLFile(FILE_RDF_STRUCTURE); //Set what XML file to parse
        $parse_structure->startParse(); //Start parsing the document
    }

}
?>